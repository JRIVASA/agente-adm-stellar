Attribute VB_Name = "modAgente"
Declare Function ExitWindowsEx& Lib "user32" (ByVal uFlags&, ByVal dwReserved&)

Private Declare Function CopyFile Lib "kernel32" Alias "CopyFileA" _
    (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Long) As Long

Private Declare Function MoveFile Lib "kernel32" Alias "MoveFileA" _
    (ByVal lpExistingFileName As String, ByVal lpNewFileName As String) As Long

Private Declare Function GetComputerName Lib "kernel32" _
    Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long
    
Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, _
        ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long

Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Enum eEstadoSucursal
    eSucNoActiva
    eSucActiva
End Enum

Enum eTipoTablaSincro
    etblMaestra
    etblTransaccion
    etblPendiente
End Enum

Enum eTipoCampo
    etblBusqueda
    etblExcepcion
End Enum

Enum eTipoError
    ErrGenerando
    ErrActualizando
    ErrOtros
End Enum

Enum eTipoActualizacion
    etaSoloAdm
    etaAdmPos
    etaSoloPos
End Enum

Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Type StrucConex
    sArchivo                                                                            As String
    sServidor                                                                           As String
    sUsuario                                                                            As String
    sPassword                                                                           As String
    sBD                                                                                 As String
    sBD_POS                                                                             As String
    sConnectTimeOut                                                                     As Long
    sCommandTimeOut                                                                     As Long
End Type

Private mConexion                                                                       As ADODB.Connection
Public mEstrucConex                                                                    As StrucConex

Public gSucursalPrincipal                                                               As String
Public gEquipo                                                                          As String
Public gCarpetaDatos                                                                    As String

Public DebugMode                                                                        As Boolean
Public DebugMode2                                                                       As Boolean

Public CompatibilidadSistemaExterno                                                     As Boolean

Global Const gCodProducto = 859
Global Const gNombreProducto = "Stellar BUSINESS"
Global gPK As String

Global TmpErrorDesc As String
Global TmpErrorNumber As Long
Global TmpErrorSource As String

Const SWP_NOSIZE = &H1
Const SWP_NOMOVE = &H2
Const SWP_NOZORDER = &H4
Const SWP_NOREDRAW = &H8
Const SWP_NOACTIVATE = &H10
Const SWP_DRAWFRAME = &H20
Const SWP_SHOWWINDOW = &H40
Const SWP_HIDEWINDOW = &H80
Const SWP_NOCOPYBITS = &H100
Const SWP_NOREPOSITION = &H200

Global POS_Sin_ProcesoDeHabladores As Boolean
Global CarExt_ReplicarCambiosProductoPadreInstanciaLocal    As Boolean
Global ManejoLoteyFechaVenc_Plantilla                       As Dictionary
Global ManejoLoteyFechaVenc_Plantilla_StrInSQL              As String

'***************************************** Rutinas Api ******************************************************************
Public Function ObtenerConfiguracion(sIniFile As String, sSection As String, _
sKey As String, sDefault As String) As String
    
    Const ParamMaxLength = 10000
    
    Dim sTemp As String * ParamMaxLength
    Dim nLength As Integer
    
    sTemp = Space$(ParamMaxLength)
    
    nLength = GetPrivateProfileString(sSection, sKey, sDefault, sTemp, ParamMaxLength, sIniFile)
    
    ObtenerConfiguracion = Left$(sTemp, nLength)
    
End Function

Public Sub EscribirConfiguracion(sIniFile As String, sSection As String, _
sKey As String, sDefault As String)
    WritePrivateProfileString sSection, sKey, sDefault, sIniFile
End Sub

Public Function CadenasIguales(ByVal pCad1 As String, ByVal pCad2 As String) As Boolean
    CadenasIguales = Trim(UCase(pCad1)) = Trim(UCase(pCad2))
End Function

Public Function ObtenerNombreEquipo() As String
    
    Dim sEquipo As String * 255
    
    GetComputerName sEquipo, 255
    ObtenerNombreEquipo = Replace(sEquipo, Chr(0), "")
    
End Function

Public Function MoverArchivo(pOrigen As String, pDestino As String) As Boolean
    'Debug.Print pOrigen & "->" & pDestino
    MoverArchivo = MoveFile(pOrigen, pDestino)
End Function

Public Function CopiarArchivo(pOrigen As String, pDestino As String, pSobreEscribir As Boolean)
    'MsgBox pOrigen & "->" & pDestino
    CopiarArchivo = CopyFile(pOrigen, pDestino, IIf(pSobreEscribir, 0, 1))
End Function

'***************************************************************Metodos Principales ***************************************

Sub Main()
    
    gPK = Chr(83) & Chr(81) & Chr(76) & "_" _
    & Chr(51) & Chr(55) & Chr(73) & Chr(51) _
    & Chr(88) & Chr(50) & Chr(49) & Chr(83) _
    & Chr(78) & Chr(68) & Chr(65) & Chr(74) _
    & Chr(68) & Chr(75) & Chr(87) & "-" _
    & Chr(57) & Chr(48) & Chr(72) & Chr(71) _
    & Chr(52) & Chr(50) & Chr(48) & "_" & Chr(90)
    
    IniciarConfiguracion
    IniciarAgente
    
End Sub

Private Sub IniciarConfiguracion()
    
    With mEstrucConex
        
        .sArchivo = App.Path & "\Setup.ini"
        
        .sCommandTimeOut = Val(ObtenerConfiguracion(.sArchivo, "Server", "CommandTimeOut", "30"))
        .sConnectTimeOut = Val(ObtenerConfiguracion(.sArchivo, "Server", "ConnectionTimeOut", "15"))
        
        .sServidor = ObtenerConfiguracion(.sArchivo, "Server", "SRV_LOCAL", "")
        .sBD = ObtenerConfiguracion(.sArchivo, "Server", "BD", "VAD10")
        .sBD_POS = ObtenerConfiguracion(.sArchivo, "Server", "BD_POS", "VAD20")
        
        .sUsuario = ObtenerConfiguracion(.sArchivo, "Server", "User", "SA")
        .sPassword = ObtenerConfiguracion(.sArchivo, "Server", "Password", "")
        
        If Not (UCase(.sUsuario) = "SA" And Len(.sPassword) = 0) Then
            
            Dim mClsTmp As Object
            
            Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
            
            If Not mClsTmp Is Nothing Then
                .sUsuario = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sUsuario)
                .sPassword = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sPassword)
            End If
            
        End If
        
        gSucursalPrincipal = ObtenerConfiguracion(.sArchivo, "Entrada", "DefaultBranch", "")
        gEquipo = ObtenerNombreEquipo()
        gCarpetaDatos = ObtenerConfiguracion(.sArchivo, "Entrada", "CarpetaDatos", App.Path & "\Datos\")
        
        If Val(ObtenerConfiguracion(.sArchivo, "Entrada", "DebugMode", "0")) = 1 _
        Or UCase(Command()) Like UCase("*DebugMode*") Then
            DebugMode = True
        End If
        
        If Val(ObtenerConfiguracion(.sArchivo, "Entrada", "DebugMode2", "0")) = 1 _
        Or UCase(Command()) Like UCase("*DebugMode2*") Then
            DebugMode2 = True
        End If
        
        CompatibilidadSistemaExterno = Val(ObtenerConfiguracion(.sArchivo, "Entrada", "CompatibilidadSistemaExterno", "0")) = 1
        
    End With
    
    If DebugMode Then
        MsgBox CleanMsg("gSucP:" & gSucursalPrincipal & vbNewLine & _
        "gEquipo:" & gEquipo & vbNewLine & _
        "gCarpetaDatos:" & gCarpetaDatos)
    End If
    
End Sub

Private Sub IniciarAgente()
    
    Dim mClsAgente As clsAgente, NewUser As String, NewPassword As String
    
    With mEstrucConex
        
        If DebugMode Then
            MsgBox "Estableciendo Conexion"
        End If
        
        If EstablecerConexion(mConexion, , , .sConnectTimeOut, , , _
        .sUsuario, .sPassword, True, NewUser, NewPassword, .sCommandTimeOut) Then
            
            If DebugMode Then
                MsgBox "Conexion exitosa" & vbNewLine & _
                "mConexion:" & Replace(mConexion.ConnectionString, _
                "Password=" & .sPassword & ";", "Password=N/A;", , , vbTextCompare)
            End If
            
            If NewUser <> "" Or NewPassword <> "" Then
                With mEstrucConex
                    EscribirConfiguracion .sArchivo, "Server", "User", NewUser
                    EscribirConfiguracion .sArchivo, "Server", "Password", NewPassword
                End With
            End If
            
            Set mClsAgente = New clsAgente
            mClsAgente.IniciarAgente mConexion
            Set mClsAgente = Nothing
            
        End If
        
    End With
    
    End
    
End Sub

'*********************************************** Funciones de Apoyo ****************************************************************

Public Function EstablecerConexion(ByRef pCn As ADODB.Connection, _
Optional pServidor As String = "", Optional pNombreBD As String = "", _
Optional pCnnTOut As Long = 15, Optional pCorrelativo As String, Optional pSucursal As String, _
Optional ByRef pUser, Optional ByRef pPassword, Optional AuthReset As Boolean = False, _
Optional ByRef NewUser As String, Optional ByRef NewPassword As String, _
Optional ByVal pCommandTimeout As Long = -1) As Boolean
    
Retry:
    
    On Error GoTo ErrHandler
    
    Dim mCadena As String
    Dim mServidor As String
    
    Set pCn = New ADODB.Connection
    pCn.ConnectionTimeout = pCnnTOut
    
    If (IsMissing(pUser) Or IsMissing(pPassword)) Then
        pCn.Open CadenaConexion(pServidor, pCnnTOut, , , pNombreBD)
    Else
        pCn.Open CadenaConexion(pServidor, pCnnTOut, CStr(pUser), CStr(pPassword), pNombreBD)
    End If
    
    If pCommandTimeout <> -1 Then
        pCn.CommandTimeout = pCommandTimeout
    End If
    
    EstablecerConexion = True
    
    Exit Function
    
ErrHandler:
    
    mErrDesc = Err.Description
    mErrNumber = Err.Number
    mErrSrc = Err.Source
    
    If AuthReset Then
        
        If Err.Number = -2147217843 Then
                    
            Dim mClsTmp As Object
            
            Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
            
            If mClsTmp Is Nothing Then GoTo UnhandledErr
            
            TmpVar = mClsTmp.RequestAccess(gCodProducto, gNombreProducto, gPK)
            
            If Not IsEmpty(TmpVar) Then
                pUser = TmpVar(0): pPassword = TmpVar(1)
                NewUser = TmpVar(2): NewPassword = TmpVar(3)
                Resume Retry
            End If
            
            Set mClsTmp = Nothing
            
        End If
        
    End If
    
UnhandledErr:
    
    GrabarErrorBD "EstablecerConexion", mErrDesc, mErrNumber, ErrOtros, pCorrelativo, pSucursal
    
End Function

Private Function CadenaConexion(pServidor As String, _
Optional pCnnTOut As Long = 0, _
Optional ByVal pUser, _
Optional ByVal pPassword, _
Optional ByVal pNombreBD As String = "")
    
    If pNombreBD = Empty Then pNombreBD = mEstrucConex.sBD
    
    Dim TempUser As String, TempPassword As String
    
    With mEstrucConex
        
        If IsMissing(pUser) Or IsMissing(pPassword) Then
            TempUser = .sUsuario: TempPassword = .sPassword
        Else
            TempUser = CStr(pUser): TempPassword = (pPassword)
        End If
        
        CadenaConexion = "Provider=SQLOLEDB.1;Initial Catalog=" & pNombreBD & ";Data Source=" & IIf(Trim(pServidor) <> "", pServidor, .sServidor) & ";" _
        & IIf(TempUser = vbNullString Or TempPassword = vbNullString, _
        "Persist Security Info=False;User ID=" & TempUser & ";", _
        "Persist Security Info=True;User ID=" & TempUser & ";Password=" & TempPassword & ";")
        
    End With
    
    'Debug.Print CadenaConexion
    
End Function

Public Function Correlativo(pCn As ADODB.Connection, pCampo As String) As String
    
    Dim mRs As ADODB.Recordset
    Dim mSQL As String
    Dim mValor As String
    
    On Error GoTo Errores
    
    mSQL = "SELECT * FROM MA_CORRELATIVOS " & _
    "WHERE cu_Campo = '" & pCampo & "' "
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSQL, pCn, adOpenDynamic, adLockPessimistic, adCmdText
    
    If Not mRs.EOF Then
        
        mRs!nu_Valor = mRs!nu_Valor + 1
        
        mValor = Format(mRs!nu_Valor, mRs!cu_Formato)
        
        mRs.Update
        
    End If
    
    mRs.Close
    
    Correlativo = mValor
    
    Exit Function
    
Errores:
    
    EscribirLog Err.Description, Err.Number
    
End Function

Public Function SucursalManejaPos(pCn As ADODB.Connection) As Boolean
    
    Dim mRs As ADODB.Recordset
    
    Sql = "SELECT Maneja_POS FROM REGLAS_COMERCIALES"
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open Sql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        SucursalManejaPos = mRs!Maneja_POS
    End If
    
    mRs.Close
    
    Set mRs = Nothing
    
End Function

Public Function SucursalManejaSucursales(pCn As ADODB.Connection) As Boolean

    Dim mRs As ADODB.Recordset
    
    Sql = "SELECT * FROM MA_SUCURSALES WHERE c_Estado = 1"
    Set mRs = New ADODB.Recordset
    
    mRs.Open Sql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    SucursalManejaSucursales = Not mRs.EOF
    
    mRs.Close
    Set mRs = Nothing
    
End Function

Public Sub EscribirLog(ByVal pMensaje As String, Optional ByVal pNumError As Long)
    
    Dim mCanal As Integer
    Dim mArchivo As String
    
    mCanal = FreeFile()
    
    If DebugMode Then
        MsgBox "Escribiendo log en " & vbNewLine & _
        App.Path & "\" & App.Title & ".log"
    End If
    
    Open App.Path & "\" & App.Title & ".log" For Append Access Write As #mCanal
    
    Print #mCanal, Now & ", " & pMensaje & IIf(pNumError > 0, " (Error Numero: " & pNumError & ")", "")
    Close #mCanal
    
End Sub

Public Sub VerificarExisteCarpeta(pCarpeta As String)
    If Dir(pCarpeta, vbDirectory) = "" Then
        MkDir pCarpeta
    End If
End Sub

Public Sub GrabarErrorBD(ByVal pRutina As String, ByVal pError As String, _
ByVal pNError As Long, ByVal pTipo As eTipoError, _
Optional ByVal pCorrida As String, _
Optional ByVal pSucursal As String, _
Optional ByVal pTabla As String, _
Optional ByVal pCodigoDoc As String, _
Optional ByVal pConcepto As String, _
Optional ByVal pFecha As Date = "01/01/1900")
    
    Dim mSQL As String
    
    On Error GoTo Errores
    
    modAgente.EscribirLog "Error " & pRutina & _
    IIf(Len(Trim(pCorrida)) > 0, "(Num_Archivo:" & pCorrida & ")", Empty) & _
    IIf(Len(Trim(pSucursal)) > 0, "(Sucursal:" & pSucursal & ")", Empty) & _
    IIf(Len(Trim(pTabla)) > 0, "(Tabla:" & pTabla & ")", Empty) & _
    IIf(Len(Trim(pCodigoDoc)) > 0, "(ID_PK_Registro:" & pCodigoDoc & ")", Empty) & _
    IIf(Len(Trim(pConcepto)) > 0, "(Concepto:" & pConcepto & ")", Empty) & _
    IIf(FechaBD(pFecha) <> "19000101", "(Fecha:" & FormatDateTime(pFecha, vbGeneralDate) & ")", Empty) & _
    ":" & vbNewLine & pError & " " & "(" & pNError & ")"
    
    mSQL = "INSERT INTO MA_ERRORCORRIDAS_AGENTE_ADM (Equipo, Corrida, Error, nError, Sucursal, " & _
    "Tabla, Codigo, Concepto, Fecha, Rutina, Tipo) VALUES " & _
    "('" & gEquipo & "', '" & pCorrida & "', '" & Left(Replace(pError, "'", ""), 120) & _
    "', " & pNError & ", '" & pSucursal & "', '" & pTabla & "', '" & pCodigoDoc & _
    "', '" & pConcepto & "', '" & FechaBD(pFecha, FBD_FULL) & "', '" & pRutina & "', " & pTipo & ")"
    
    If DebugMode Then
        MsgBox "Grabando Error." & vbNewLine & vbNewLine & mSQL
    End If
    
    mConexion.Execute mSQL
    
    Exit Sub
    
Errores:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    If DebugMode Then
        MsgBox "Error Grabando Errores..." & vbNewLine & _
        mErrorNumber & vbNewLine & _
        mErrorDesc & vbNewLine & _
        mErrorSource
    End If
    
    modAgente.EscribirLog "Error Grabando Tabla Errores: " & Err.Description, Err.Number
    Err.Clear
    
End Sub

Public Function NombreArchivo(pTabla As String, pCorrelativo As String, _
Optional pCarpeta As String = "") As String
    
    Dim mDirectorio As String
    
    'Debug.Print Len(pCarpeta)
    'Debug.Print Mid(pCarpeta, Len(pCarpeta), Len(pCarpeta))
    
    'mDirectorio = IIf(pCarpeta <> "", pCarpeta, App.Path) & "\"
    
    mDirectorio = IIf(pCarpeta <> "", pCarpeta, App.Path)
    mDirectorio = mDirectorio & IIf(Mid(mDirectorio, Len(mDirectorio), Len(mDirectorio)) = "\", "", "\")
    
    NombreArchivo = mDirectorio & pTabla & ";" & pCorrelativo & ".adtg"
    'MsgBox NombreArchivo
    
End Function

Public Function NombreTablaArchivo(pArchivo As String, ByRef pTabla As String, ByRef pCorrelativo As String) As Boolean
    
    Dim mAux As Variant
    Dim mArchivo As String
    
    On Error GoTo Errores
    ' c:\datos\nombre;correlativo.adtg
    mAux = Split(pArchivo, "\")
    mArchivo = mAux(UBound(mAux)) 'nombre;correlativo.adtg
    mAux = Split(mArchivo, ";")
    pTabla = mAux(0) 'nombre
    pCorrelativo = Left(mAux(1), InStr(1, mAux(1), ".") - 1) ' correlativo
    NombreTablaArchivo = True
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Public Function BuscarArchivosActualizar(pRutaBusqueda As String, _
Optional pCorrelativo, _
Optional pMover As Boolean) As Collection
    
    Dim mArchivos As Collection
    Dim mArchivo As String
    
    Set mArchivos = New Collection
    
    If IsMissing(pCorrelativo) Then
        If pMover Then
            mArchivo = Dir(pRutaBusqueda & "\*.adtg")
        Else
            mArchivo = Dir(pRutaBusqueda & "\*pend*.adtg")
        End If
    Else
        If pMover Then
            mArchivo = Dir(pRutaBusqueda & "\*" & pCorrelativo & ".adtg")
        Else
            mArchivo = Dir(pRutaBusqueda & "\*pend*" & pCorrelativo & ".adtg")
        End If
    End If
    
    Do While mArchivo <> ""
        mArchivos.Add mArchivo
        mArchivo = Dir
    Loop
    
    Set BuscarArchivosActualizar = mArchivos
    
End Function

Public Function BuscarTablaActualizar(pTbl As clsTablasSincronizar, pTipo As eTipoTablaSincro) As clsTablaSincro
    
    Dim mTbl As clsTablaSincro
    
    For Each mTbl In pTbl.Tablas
        If mTbl.Tipo = pTipo Then
            Set BuscarTablaActualizar = mTbl
            Exit Function
        End If
    Next
    
End Function

Public Function AbrirRsActualizar(pNombre As String, Optional pAsc As Boolean = True) As ADODB.Recordset
    
    Dim mRs As ADODB.Recordset
    
    Set mRs = New ADODB.Recordset
    
    If ExisteRsActualizar(pNombre) Then
        
        mRs.Open pNombre, "Provider=MSPersist;"
        
        If Not mRs.EOF Then
            If ExisteCampoRs(mRs, "ID") Then
                mRs.Sort = "ID " & IIf(pAsc, "", " Desc ")
            Else
                If ExisteCampoRs(mRs, "n_ID") Then mRs.Sort = "n_ID" & IIf(pAsc, "", " Desc ")
            End If
        End If
        
        Set AbrirRsActualizar = mRs
        
    End If
    
End Function

Public Function RsContieneDatos(pRs As ADODB.Recordset) As Boolean
    If Not pRs Is Nothing Then
        RsContieneDatos = Not pRs.EOF
    End If
End Function

Public Function BuscarMaxId(pRsPend As ADODB.Recordset) As Long
    pRsPend.Sort = "id desc "
    BuscarMaxId = pRsPend!ID
End Function

Public Function ExisteTabla(pTabla As String, pCn As ADODB.Connection, Optional pBD As String = "") As Boolean
    On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Table_Name FROM " & mBD & "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" & pTabla & "'")
    ExisteTabla = Not (pRs.EOF And pRs.BOF)
End Function

Public Function ExisteCampoRs(pRs As ADODB.Recordset, pCampo As String) As Boolean
    
    On Error GoTo Errores
    
    a = pRs.Fields(pCampo).Value
    ExisteCampoRs = True
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Public Function ExisteRsActualizar(pNombre As String) As Boolean
    ExisteRsActualizar = Dir(pNombre, vbArchive) <> ""
End Function

Public Function BuscarRsSucursalEmpresa(pCodigo As String) As ADODB.Recordset
    
    Dim mRs As New ADODB.Recordset
    
    On Error GoTo Errores
    mSQL = "SELECT cs_CodLocalidad FROM MA_SUCURSALES_EMPRESA WHERE cs_CodSucursal = '" & pCodigo & "'"
    mRs.Open mSQL, mConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Set BuscarRsSucursalEmpresa = mRs
    
Errores:
    
    'Debug.Print Err.Description
    
    Err.Clear
    
End Function

Public Sub VentanaVisible(ByRef pFrm As Object, Optional pTop As Boolean = False)
    If pTop Then
        'SetWindowPos pFrm.hWnd, -1, pFrm.Left / Screen.TwipsPerPixelX, pFrm.Top / Screen.TwipsPerPixelY, pFrm.Width / Screen.TwipsPerPixelX, pFrm.Height / Screen.TwipsPerPixelY, SWP_NOACTIVATE Or SWP_NOSIZE Or SWP_NOMOVE
        SetWindowPos pFrm.hWnd, -1, 0, 0, 0, 0, SWP_NOACTIVATE Or SWP_NOSIZE
    Else
        SetWindowPos pFrm.hWnd, -2, 0, 0, 0, 0, SWP_NOACTIVATE Or SWP_NOSIZE
    End If
End Sub

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = "" Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Function FechaBD(ByVal Expression, Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim dDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
            
            Case FBD_Fecha
                
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
                
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYY-MM-DD HH:mm:ss") '& Format(lMilli / 1000, ".000")
                
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
                
                FechaBD = Format(Expression, "HH:mm")
                
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function

Public Function SplitStringIntoFixedLengthLines(ByVal MaxLineChars As Long, _
ByVal pFullString As String, _
Optional ByVal RemoveLf As Boolean = True)
    
    Dim i As Long, Length As Long, Tmp As String, OverRun As String, RemainingCharsNum As Long, FoundLine As Boolean
    
    i = 1
    Tmp = vbNullString
    OverRun = vbNullString
    Length = Len(pFullString)
    
    While (i < Length)
        
        RemainingCharsNum = Length - i
        
        If RemainingCharsNum > MaxLineChars Then
            Tmp = Mid(pFullString, i, MaxLineChars)
            OverRun = WordWrapLineBySpace(Tmp, MaxLineChars, FoundLine)
        Else
            Tmp = Mid(pFullString, Length - RemainingCharsNum)
            OverRun = vbNullString
        End If
        
        SplitStringIntoFixedLengthLines = SplitStringIntoFixedLengthLines & IIf(SplitStringIntoFixedLengthLines = 0, vbNullString, vbNewLine) & (IIf(FoundLine, Mid(Tmp, 1, Len(Tmp) - 1), Tmp))
        
        i = i + MaxLineChars - Len(OverRun)
        
    Wend
    
    If RemoveLf Then SplitStringIntoFixedLengthLines = Replace(SplitStringIntoFixedLengthLines, vbLf, vbNullString)
    
End Function

Public Function WordWrapLineBySpace(ByRef LineStr As String, MaxLineChars As Long, ByRef FoundLine As Boolean)
    
    Dim WhatIsLeft As String, Index As Long, CalculatedString As String
    
    WhatIsLeft = vbNullString
    
    Index = InStr(LineStr, vbNewLine)
    
    If Index > 0 Then
        
        CalculatedString = Mid(LineStr, 1, Index)
        WhatIsLeft = Mid(LineStr, Index + 1)
        FoundLine = True
        
    Else
        
        FoundLine = False
        
        Index = InStrRev(LineStr, " ")
        
        If Index > 0 Then
            CalculatedString = Mid(LineStr, 1, Index)
            WhatIsLeft = Mid(LineStr, Index + 1)
        Else
            CalculatedString = Mid(LineStr, 1, MaxLineChars)
            WhatIsLeft = Mid(LineStr, MaxLineChars + 1)
        End If
        
    End If
    
    LineStr = CalculatedString
    WordWrapLineBySpace = WhatIsLeft
    
End Function

' NO USAR Hex2Dec! No funciona con Montos Altos / muchos caracteres decimales.
' Usar ahora:

Public Function HexToDec(ByVal pHexNum As String) As Double
    
    On Error GoTo Error
    
    Dim DblOut As Double
    
    Dim i As Long
    Dim C As Long
    
    For i = 1 To Len(pHexNum)
        
        C = Asc(UCase(Mid(pHexNum, i, 1)))
        
        Select Case C
            
            Case 65 To 70
              DblOut = DblOut + ((C - 55) * 16 ^ (Len(pHexNum) - i))
            
            Case 48 To 57
              DblOut = DblOut + ((C - 48) * 16 ^ (Len(pHexNum) - i))
            
            Case Else
            
        End Select
        
    Next i
    
    HexToDec = DblOut
    
    Exit Function
    
Error:
    
    HexToDec = 0
    
End Function

Public Function HexIBM(ByVal pHexByte As String): HexIBM = Chr(CLng(HexToDec(pHexByte))): End Function

' Estos car�cteres no salen en un MsgBox y no se muestra el resto de la cadena.
Public Function CleanMsg(ByVal pMsg As String) As String
    CleanMsg = ReplaceMultiple(pMsg, vbNullString, HexIBM("00"))
End Function

Public Function ReplaceMultiple(ByVal OrigString As String, _
     ByVal ReplaceString As String, ParamArray FindChars()) _
     As String

    '*********************************************************
    'PURPOSE: Replaces multiple substrings in a string with the
    'character or string specified by ReplaceString
    
    'PARAMETERS: OrigString -- The string to replace characters in
    '            ReplaceString -- The replacement string
    '            FindChars -- comma-delimited list of
    '                 strings to replace with ReplaceString
    '
    'RETURNS:    The String with all instances of all the strings
    '            in FindChars replaced with Replace String
    'EXAMPLE:    s= ReplaceMultiple("H;*()ello", "", ";", ",", "*", "(", ")") -
                 'Returns Hello
    'CAUTIONS:   'Overlap Between Characters in ReplaceString and
    '             FindChars Will cause this function to behave
    '             incorrectly unless you are careful about the
    '             order of strings in FindChars
    '***************************************************************

    'Esta funci�n la busqu� para cuando hay que hacer muchos replace a un string
    'generalmente para eliminar un lote de car�cteres no soportados de un string
    'o algo as�. Aunque este es el m�todo simple pero limitado. Para algo mas seguro
    'y eficiente, conviene aprender a usar RegExp Patterns. Para un ejemplo de esto
    'Ver la funci�n ValidarRif del Agente SivPre.

    Dim lLBound As Long
    Dim lUBound As Long
    Dim lCtr As Long
    Dim sAns As String
    
    lLBound = LBound(FindChars)
    lUBound = UBound(FindChars)
    
    sAns = OrigString
    
    For lCtr = lLBound To lUBound
        sAns = Replace(sAns, CStr(FindChars(lCtr)), ReplaceString)
    Next
    
    ReplaceMultiple = sAns
        
End Function

Public Function VerificarADTGCorrupto(pRs As ADODB.Recordset, _
ByVal pMainError, ByVal pNativeError)
    
    On Error Resume Next
    
    Dim CurrentRs As ADODB.Recordset
    Dim CurrentErr As ADODB.Error
    
    Set CurrentRs = pRs
    
    If Not CurrentRs Is Nothing Then
        
        For Each CurrentErr In CurrentRs.ActiveConnection.Errors
            
            If CurrentErr.Number = pMainError _
            And CurrentErr.NativeError = pNativeError _
            Then
                
                VerificarADTGCorrupto = True
                
                Exit Function
                
            End If
            
        Next
        
    End If
    
End Function

Public Function KillSecure(ByVal pFile As String) As Boolean
    On Error GoTo KS
    Kill pFile: KillSecure = True
    Exit Function
KS:
    'Debug.Print Err.Description
    Err.Clear 'Ignorar.
End Function

Public Function KillShot(ByVal pFile As String) As Boolean
    KillShot = KillSecure(pFile)
End Function

Public Function BuscarReglaNegocioStr(pCn As ADODB.Connection, _
ByVal pCampo, _
Optional ByVal pDefault As String = "") As String
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    On Error GoTo Errores
    
    mSQL = "SELECT * FROM MA_REGLASDENEGOCIO " & _
    "WHERE Campo = '" & pCampo & "' "
    
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarReglaNegocioStr = CStr(mRs!Valor)
    Else
        BuscarReglaNegocioStr = pDefault
    End If
    
    mRs.Close
    
    Exit Function
    
Errores:
    
    Err.Clear
    
    BuscarReglaNegocioStr = pDefault
    
End Function

Public Function QuitarComillasDobles(ByVal pCadena As String) As String
    QuitarComillasDobles = Replace(pCadena, Chr(34), "")
End Function

Public Function QuitarComillasSimples(ByVal pCadena As String) As String
    QuitarComillasSimples = Replace(pCadena, Chr(39), "")
End Function

Public Function LimitarComillasSimples(ByVal pCadena As String) As String
    
    Dim Cad As String
    
    Cad = pCadena
    
    Do While (Cad Like "*''*")
        Cad = Replace(Cad, "''", "'")
    Loop
    
    LimitarComillasSimples = Cad
    
End Function

Public Function FixTSQL(ByVal pCadena As String) As String
    FixTSQL = Replace(pCadena, "'", "''")
End Function

' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If Err.Number = 0 Then Exit Sub
    pVar = pValue
End Sub

' Sintaxis para obtener un elemento �ndice de un objeto de manera segura,
' Retornando un Valor por Defecto en caso de que el elemento no exista,
' Para evitar errores en tiempo de ejecuci�n y no tener que separar las instrucciones en IFs.

' Ejemplo, Caso Recordset

' Cambiaria:

'If ExisteCampoTabla(Campo, Recordset) Then
'   Variable = Recordset!Campo
'Else
'   Variable = 0
'End If

' Por:

' Variable = sProp(Recordset, Campo, 0)
' pObj(pItem) ser�a equivalente a regresar : pObj.Fields(pItem).Value)

' Todos los par�metros tanto de Entrada/Salida son Variant (Acepta Cualquier Cosa)

Public Function SafeItem(pObj, pItem, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeItem, pObj(pItem)
    Exit Function
Default:
    SafeVarAssign SafeItem, pDefaultValue
End Function

' Mismo prop�sito que la anterior, SafeItem()
' Solo que con la sintaxis para acceder a la propiedad de un objeto
' en vez de a un elemento de una colecci�n.

' Quizas para uso futuro.

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeProp, CallByName(pObj, pProperty, VbGet)
    Exit Function
Default:
    SafeVarAssign SafeProp, pDefaultValue
End Function

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a un elemento.

Public Sub SafeItemAssign(pObj, pItem, pValue)
    On Error Resume Next
    Set pObj(pItem) = pValue
    If Err.Number = 0 Then Exit Sub
    pObj(pItem) = pValue
End Sub

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a una propiedad.

Public Sub SafePropAssign(pObj, pProperty, pValue)
    On Error Resume Next
    CallByName pObj, pProperty, VbLet, pValue
End Sub

Public Function AsEnumerable(ArrayList As Variant) As Collection
    
    Set AsEnumerable = New Collection
    
    On Error Resume Next
    
    For i = LBound(ArrayList) To UBound(ArrayList)
        AsEnumerable.Add ArrayList(i), CStr(i)
    Next i
    
End Function

'Funciones auxiliares para trabajar con Collections.

Public Function SafeEquals(ByVal pVal1, ByVal pVal2) As Boolean
    On Error Resume Next
    SafeEquals = (pVal1 = pVal2) ' Attempt to Compare Values.
    If Err.Number = 0 Then Exit Function
    SafeEquals = (pVal1 Is pVal2) ' Attempt to Compare Object References.
End Function

' Changed AS a Pointer to V2.

Public Function Collection_AddKey(pCollection As Collection, ByVal pValor, ByVal pKey As String) As Boolean
    Collection_AddKey = Collection_SafeAddKey(pCollection, pValor, pKey)
End Function

' To attempt to add Item + Key and ignore error in case Key is already contained in the collection.
' Ignore duplication error.

Public Function Collection_SafeAddKey(pCollection As Collection, ByVal pValue, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    pCollection.Add pValue, pKey
    
    Collection_SafeAddKey = True
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_EncontrarValor(pCollection As Collection, ByVal pValor, Optional ByVal pIndiceInicial As Long = 1) As Long
    Collection_EncontrarValor = Collection_FindValueIndex(pCollection, pValor, pIndiceInicial)
End Function

' How to Use:

' 1) To Know if Value Exists:

'If Collection_FindValueIndex(pCollection, pValue) <> -1 Then

' 2) To Search All Instances (Indexes) of Value in the Collection. Quick Example:

' SearchIndex = Collection_FindValueIndex(pCollection, pValue) ' Start Looking from Index 1

' If SearchIndex <> -1 Then
    ' This should be in a kind of While Loop...
    ' Keep searching other matches starting from next position:
    'SearchIndex = Collection_FindValueIndex(pCollection, pValue, SearchIndex + 1)
    ' Until Search Index = -1
' Else
    ' Next Match not found...
' End if

Public Function Collection_FindValueIndex(pCollection As Collection, ByVal pValue, Optional ByVal pStartIndex As Long = 1) As Long
    
    On Error GoTo Error
    
    Dim i As Long
    
    Collection_FindValueIndex = -1
    
    For i = pStartIndex To pCollection.Count
        If SafeEquals(pCollection.Item(i), pValue) Then
            Collection_FindValueIndex = i
            Exit Function
        End If
    Next i
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteKey(pCollection As Collection, ByVal pKey As String, Optional ByVal pItemsAreObjects As Boolean = False) As Boolean
    Collection_ExisteKey = Collection_HasKey(pCollection, pKey)
End Function

' Collection_ExisteKey V2 (Better) (pItemsAreObjects Knowledge is not required)

Public Function Collection_HasKey(pCollection As Collection, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    Dim TmpVal
    
    If IsNull(SafeItem(pCollection, pKey, Null)) Then ' Key might exist but be Null so we do another check.
        SafeVarAssign TmpVal, pCollection.Item(pKey) ' If this doesn't Proc Error Key is Null, but exists.
        Collection_HasKey = True
    Else ' Key Exists AND <> NULL
        Collection_HasKey = True
    End If
    
    Exit Function
        
Error:
    
    'Debug.Print Err.Description
    
    Collection_HasKey = False ' Only on error, Key is not contained in the Collection.
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteIndex(pCollection As Collection, ByVal pIndex As Long, Optional ByVal pItemsAreObjects As Boolean = False) As Boolean
    Collection_ExisteIndex = Collection_HasIndex(pCollection, pIndex)
End Function

' Collection_ExisteIndex V2 (Better) (pItemsAreObjects Knowledge is not required)

Public Function Collection_HasIndex(pCollection As Collection, pIndex As Long) As Boolean
    
    On Error GoTo Error
    
    Dim TmpVal
    
    If IsNull(SafeItem(pCollection, pIndex, Null)) Then ' Index might exist but be Null so we do another check.
        SafeVarAssign TmpVal, pCollection.Item(pIndex) ' If this doesn't Proc Error Index also exists.
        Collection_HasIndex = True
    Else ' Index Exists AND <> NULL
        Collection_HasIndex = True
    End If
    
    Exit Function
    
Error:
    
    'Debug.Print Err.Description
    
    Collection_HasIndex = False ' Only on error Index is not contained in the Collection.
    
End Function

Public Function Collection_RemoveKey(pCollection As Collection, pKey As String) As Boolean
    Collection_RemoveKey = Collection_SafeRemoveKey(pCollection, pKey)
End Function

' Ignore error even if the Key is not contained in the Collection.

Public Function Collection_SafeRemoveKey(pCollection As Collection, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    pCollection.Remove pKey
        
    Collection_SafeRemoveKey = True
     
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Collection_SafeRemoveKey = False ' Non - existant.
    
End Function

Public Function Collection_RemoveIndex(pCollection As Collection, pIndex As Long) As Boolean
    Collection_RemoveIndex = Collection_SafeRemoveIndex(pCollection, pIndex)
End Function

' Ignore error even if the Index is not contained in the Collection.

Public Function Collection_SafeRemoveIndex(pCollection As Collection, pIndex As Long) As Boolean
    
    On Error GoTo Error
    
    pCollection.Remove pIndex
    
    Collection_SafeRemoveIndex = True
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Collection_SafeRemoveIndex = False ' Non - existant.
    
End Function

Public Function ConvertirCadenaDeAsociacion(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pKeysCaseOption As Integer = 0, _
Optional ByVal pValuesCaseOption As Integer = 0) As Collection
    
    ' Alternativas comunes para Separador :   ";" , ","
    
    On Error GoTo Error
    
    Set ConvertirCadenaDeAsociacion = New Collection
    
    Dim Item, ParClaveValor
    
    For Each Item In AsEnumerable(Split(pCadena, pSeparador))
        
        ParClaveValor = Split(CStr(Item), ":", 2)               'Izq:Clave        'Der:Valor
        
        If pKeysCaseOption = 1 Then ParClaveValor(0) = LCase(ParClaveValor(0))
        If pKeysCaseOption = 2 Then ParClaveValor(0) = UCase(ParClaveValor(0))
        If pKeysCaseOption = 3 Then ParClaveValor(0) = StrConv(ParClaveValor(0), vbProperCase)
        
        If pValuesCaseOption = 1 Then ParClaveValor(1) = LCase(ParClaveValor(1))
        If pValuesCaseOption = 2 Then ParClaveValor(1) = UCase(ParClaveValor(1))
        If pValuesCaseOption = 3 Then ParClaveValor(1) = StrConv(ParClaveValor(1), vbProperCase)
        
        Call Collection_SafeAddKey(ConvertirCadenaDeAsociacion, ParClaveValor(1), ParClaveValor(0))
        
    Next
    
    If ConvertirCadenaDeAsociacion.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaDeAsociacion = Nothing
    
End Function

Public Function ConvertirCadenaDeAsociacionAvanzado(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pSeparadorInterno As String = ":", _
Optional ByVal pKeysCaseOption As Integer = 0, _
Optional ByVal pValuesCaseOption As Integer = 0) As Dictionary
    
    ' Alternativas comunes para Separador :   ";" , ","
    ' Alternativas comunes para Separador Interno:  ":", "="
    
    On Error GoTo Error
    
    Set ConvertirCadenaDeAsociacionAvanzado = New Dictionary
    
    Dim Item, ParClaveValor
    
    For Each Item In AsEnumerable(Split(pCadena, pSeparador))
        
        ParClaveValor = Split(CStr(Item), pSeparadorInterno, 2) ' Izq -> Clave ' Der -> Valor
        
        If pKeysCaseOption = 1 Then ParClaveValor(0) = LCase(ParClaveValor(0))
        If pKeysCaseOption = 2 Then ParClaveValor(0) = UCase(ParClaveValor(0))
        If pKeysCaseOption = 3 Then ParClaveValor(0) = StrConv(ParClaveValor(0), vbProperCase)
        
        If pValuesCaseOption = 1 Then ParClaveValor(1) = LCase(ParClaveValor(1))
        If pValuesCaseOption = 2 Then ParClaveValor(1) = UCase(ParClaveValor(1))
        If pValuesCaseOption = 3 Then ParClaveValor(1) = StrConv(ParClaveValor(1), vbProperCase)
        
        If Not ConvertirCadenaDeAsociacionAvanzado.Exists(ParClaveValor(0)) Then
            ConvertirCadenaDeAsociacionAvanzado.Add ParClaveValor(0), ParClaveValor(1)
        End If
        
    Next
    
    If ConvertirCadenaDeAsociacionAvanzado.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaDeAsociacionAvanzado = Nothing
    
End Function

Public Function ExportarCadenaDeAsociacion(ByVal pCollection As Collection, _
Optional ByVal pSeparador As String = "|") As String
    
    ' Alternativas comunes para Separador :   ";" , ","
    
    On Error GoTo Error
    
    ExportarCadenaDeAsociacion = Empty
    
    Dim Item
    
    If pCollection.Count <= 0 Then Exit Function
    
    For Each Item In pCollection
        ExportarCadenaDeAsociacion = ExportarCadenaDeAsociacion & _
        pSeparador & Item
    Next
    
    ExportarCadenaDeAsociacion = Mid(ExportarCadenaDeAsociacion, 2)
    
    Exit Function
    
Error:
    
    ExportarCadenaDeAsociacion = Empty
    
End Function

Public Function ExportarCadenaDeAsociacionAvanzado(ByVal pValues As Dictionary, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pSeparadorInterno As String = ":") As String
    
    ' Alternativas comunes para Separador :   ";" , ","
    ' Alternativas comunes para Separador Interno:  ":", "="
    
    On Error GoTo Error
    
    ExportarCadenaDeAsociacionAvanzado = Empty
    
    Dim ItemKey, KeysCollection
    
    Set KeysCollection = AsEnumerable(pValues.Keys())
    
    If KeysCollection.Count <= 0 Then Exit Function
    
    For Each ItemKey In KeysCollection
        ExportarCadenaDeAsociacionAvanzado = ExportarCadenaDeAsociacionAvanzado & _
        pSeparador & ItemKey & pSeparadorInterno & pValues.Item(ItemKey)
    Next
    
    ExportarCadenaDeAsociacionAvanzado = Mid(ExportarCadenaDeAsociacionAvanzado, 2)
    
    Exit Function
    
Error:
    
    ExportarCadenaDeAsociacionAvanzado = Empty
    
End Function

' Imprime un recordset o el Record Actual utilizando el m�todo original de ADODB.

' El Original de ADODB pero incluyendo los headers.
' Es m�s r�pido pero menos ordenado.
' NO Deja el recordset en su posici�n del cursor original:
' Lo lleva al EOF si es el Recordset, o avanza el cursor una posici�n si es solo el Record Actual.
' Por lo cual habr�a que hacer MoveFirst o MovePrevious respectivamente
' Dependiendo del tipo de recorset para poder volver a recorrerlo.

Public Function QuickPrintRecordset(ByVal pRs As ADODB.Recordset, Optional OnlyCurrentRow As Boolean = False) As String
    
    On Error Resume Next
    
    Dim Data As Boolean
    Dim RsPreview As String
    Dim RowData As String
    
    Data = Not (pRs.BOF And pRs.EOF)
    
    For i = 0 To pRs.Fields.Count - 1
        RsPreview = RsPreview + IIf(i = 0, vbNullString, "|") + pRs.Fields(i).Name
    Next i
    
    RsPreview = RsPreview & vbNewLine
    
    If Data Then
        If OnlyCurrentRow Then
            RowData = pRs.GetString(, 1, "|", vbNewLine, "NULL")
        Else
            RowData = pRs.GetString(, , "|", vbNewLine, "NULL")
        End If
    End If
    
    RsPreview = RsPreview & RowData
    
    QuickPrintRecordset = RsPreview
    
End Function

Public Function QuickPrintRecord(ByVal pRs As ADODB.Recordset) As String
    QuickPrintRecord = QuickPrintRecordset(pRs, True)
End Function

' Imprime un Recordset o el Record Actual de manera ordenada. Deja al recordset en su posici�n (Record) Original.

' Parametros:

' 1) pRs: Recordset
' 2) pRowNumAlias: Alias para una columna que enumera los rows. Si no se especifica o OnlyCurrentRow no se imprime
' 3) OnlyCurrentRow: Imprimir solo el record actual o todo.
' 4) EncloseHeaders: Encierra los cabeceros en Corchetes para mayor separaci�n.
' 5) LineBeforeData: Agrega una Linea de separaci�n entre Header y Records.
' 6) pOnValueError: Que se imprime si da un error a la hora de recuperar un campo.
' 7) Uso interno: Dejar siempre el valor default. Es para poder imprimir los Recordsets anidados (Tipo Consulta Shape).

' Recomendaci�n al obtener el String devuelto: Copiar y Pegar en TextPad o un Bloc de Notas sin Line Wrap para analizar.

Public Function PrintRecordset(ByVal pRs As ADODB.Recordset, Optional ByVal pRowNumAlias As String = vbNullString, _
Optional OnlyCurrentRow As Boolean = False, Optional EncloseHeaders As Boolean = True, _
Optional LineBeforeData As Boolean = False, Optional ByVal pOnValueError As String = "Error|N/A", _
Optional NestLevel As Byte = 0) As String
    
    On Error GoTo FuncError
    
    Dim RsPreview As String, bRowNumAlias As Boolean, RowNumAlias As String, RowNumCount As Double
    Dim OriginalRsPosition, TmpRsValue, mColLength() As Double, mColArrIndex, pRsValues As ADODB.Recordset
    Dim Data As Boolean
    
    If OnlyCurrentRow Then pRowNumAlias = vbNullString
    
    bRowNumAlias = (pRowNumAlias <> vbNullString)
    
    Data = Not (pRs.BOF And pRs.EOF)
    
    ReDim mColLength(pRs.Fields.Count - 1 + IIf(bRowNumAlias, 1, 0))
    
    If bRowNumAlias Then
        RowNumAlias = IIf(EncloseHeaders, "[", vbNullString) & CStr(pRowNumAlias) & IIf(EncloseHeaders, "]", vbNullString)
        RowNumCount = 0
        RsPreview = RsPreview & GetTab(NestLevel) & "$(mCol[0])"
        mColLength(0) = Len(RowNumAlias)
    End If
    
    For i = 0 To pRs.Fields.Count - 1
        'RsPreview = RsPreview + pRs.Fields(i).Name + vbTab
        mColArrIndex = IIf(bRowNumAlias, i + 1, i)
        RsPreview = RsPreview & IIf(Not bRowNumAlias, GetTab(NestLevel), vbNullString) & "$(mCol[" & mColArrIndex & "])"
        mColLength(mColArrIndex) = Len(IIf(EncloseHeaders, "[", vbNullString) & pRs.Fields(i).Name & IIf(EncloseHeaders, "]", vbNullString))
    Next i
    
    If LineBeforeData Then RsPreview = RsPreview & vbNewLine
    
    If Data And Not OnlyCurrentRow Then
        OriginalRsPosition = SafeProp(pRs, "Bookmark", -1)
        If pRs.CursorType <> adOpenForwardOnly Then
            pRs.MoveFirst
        Else
            pRs.Requery
        End If
        If OriginalRsPosition <> -1 Then Set pRsValues = pRs.Clone(adLockReadOnly)
    Else
        Set pRsValues = pRs
    End If
    
    RowNumCount = 0
    
    Do While Not pRs.EOF
        
        'RsPreview = RsPreview & vbNewLine
        'If bRowNumAlias Then RsPreview = RsPreview & Rellenar_SpaceR(RowNumCount, Len(RowNumAlias), " ")
        
        If bRowNumAlias Then
            TmpRsValue = RowNumCount
            If Len(TmpRsValue) > mColLength(0) Then mColLength(0) = Len(TmpRsValue)
            'RsPreview = RsPreview & TmpRsValue
        End If
        
        For i = 0 To pRs.Fields.Count - 1
            TmpRsValue = isDBNull(SafeItem(pRs, pRs.Fields(i).Name, pOnValueError), "NULL")
            mColArrIndex = IIf(bRowNumAlias, i + 1, i)
                        
            If UCase(TypeName(TmpRsValue)) <> UCase("Recordset") Then
                If Len(TmpRsValue) > mColLength(mColArrIndex) Then mColLength(mColArrIndex) = Len(TmpRsValue)
            End If
            'RsPreview = RsPreview & TmpRsValue
        Next i
        
        RowNumCount = RowNumCount + 1
        
        If OnlyCurrentRow Then Exit Do
        
        pRs.MoveNext
        
    Loop
    
    If bRowNumAlias Then RsPreview = Replace(RsPreview, "$(mCol[0])", Rellenar_SpaceR(RowNumAlias, mColLength(0), " ") & vbTab, , 1)
    
    For i = 0 To pRs.Fields.Count - 1
        mColArrIndex = IIf(bRowNumAlias, i + 1, i)
        RsPreview = Replace(RsPreview, "$(mCol[" & mColArrIndex & "])", Rellenar_SpaceR( _
        IIf(EncloseHeaders, "[", vbNullString) & pRs.Fields(i).Name & IIf(EncloseHeaders, "]", vbNullString), _
        mColLength(mColArrIndex), " ") & vbTab, , 1)
    Next i
    
    If OriginalRsPosition = -1 And Not OnlyCurrentRow Then
        Set pRsValues = pRs
        If Data Then
            If pRsValues.CursorType <> adOpenForwardOnly Then
                pRsValues.MoveFirst
            Else
                pRsValues.Requery
            End If
        End If
    End If
    
    RowNumCount = 0
    
    Do While Not pRsValues.EOF
        
        RsPreview = RsPreview & GetTab(NestLevel) & vbNewLine
        
        If bRowNumAlias Then
            TmpRsValue = RowNumCount
            RsPreview = RsPreview & GetTab(NestLevel) & Rellenar_SpaceR(TmpRsValue, mColLength(0), " ") & vbTab
        End If
        
        For i = 0 To pRsValues.Fields.Count - 1
            TmpRsValue = isDBNull(SafeItem(pRsValues, pRsValues.Fields(i).Name, pOnValueError), "NULL")
            mColArrIndex = IIf(bRowNumAlias, i + 1, i)
            If Not UCase(TypeName(TmpRsValue)) = UCase("Recordset") Then
                RsPreview = RsPreview & IIf(Not bRowNumAlias, GetTab(NestLevel), vbNullString) & Rellenar_SpaceR(TmpRsValue, mColLength(mColArrIndex), " ") & vbTab
            Else
                RsPreview = RsPreview & vbNewLine & PrintRecordset(TmpRsValue, pRowNumAlias, OnlyCurrentRow, EncloseHeaders, LineBeforeData, pOnValueError, NestLevel + 1)
            End If
        Next i
        
        RowNumCount = RowNumCount + 1
        
        If OnlyCurrentRow Then Exit Do
        
        pRsValues.MoveNext
        
    Loop
    
    PrintRecordset = RsPreview
    
Finally:
    
    On Error Resume Next
    
    If Data And Not OnlyCurrentRow Then
        If OriginalRsPosition <> -1 Then
            If pRs.CursorType = adOpenForwardOnly Then
                pRs.Requery
                pRs.Move OriginalRsPosition, 0
            Else
                SafePropAssign pRs, "Bookmark", OriginalRsPosition
            End If
        Else
            If pRsValues.CursorType <> adOpenForwardOnly Then
                pRsValues.MoveFirst
            Else
                pRsValues.Requery
            End If
        End If
    End If
    
    Exit Function
    
FuncError:
    
    'Resume ' Debug
    
    PrintRecordset = vbNullString
    
    Resume Finally
    
End Function

' Imprime el Record Actual de un Recordset.

Public Function PrintRecord(ByVal pRs As ADODB.Recordset, _
Optional EncloseHeaders As Boolean = True, Optional LineBeforeData As Boolean = False, _
Optional ByVal pOnValueError As String = "Error|N/A") As String
    PrintRecord = PrintRecordset(pRs, , True, EncloseHeaders, LineBeforeData, pOnValueError)
End Function

Public Function GetLines(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyLines As Integer
    
    HowManyLines = HowMany
    
    For i = 1 To HowManyLines
        GetLines = GetLines & vbNewLine
    Next i
    
End Function

Public Function GetTab(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyTabs As Integer
    
    HowManyTabs = HowMany
    
    For i = 1 To HowManyTabs
        GetTab = GetTab & vbTab
    Next i
    
End Function

Public Function Rellenar_SpaceR(intValue, intDigits, car)
    '*** ESPACIO A LA DERECHA
    mValorLon = intDigits - Len(intValue)
    Rellenar_SpaceR = intValue & String(IIf(mValorLon < 0, intDigits, mValorLon), car)
End Function

Public Function isDBNull(ByVal pValue, Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            SafeVarAssign isDBNull, pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

Public Function ValidarConexion(pConexion As Object, _
Optional pNewConnectionString As String, _
Optional TmpCnTimeOut As Long = 4) As Boolean
    
    On Error GoTo Check
    
    ' La idea de esta funci�n es reestablecer conexiones que se asume estan
    ' en estado abierto pero internamente pudieran estar ocasionando error.
    ' Actualizaci�n: O si est� cerrada, intentar abrirla nuevamente para poder utilizarla.
    
    'Debug.Print pConexion.ConnectionString
    
    Dim mRs As ADODB.Recordset, mSQL As String, Reintento As Boolean
    Dim mConnectionString As String, mConnectionTimeOut As Long, mCommandTimeOut As Long
    
    mConnectionTimeOut = pConexion.ConnectionTimeout
    mCommandTimeOut = pConexion.CommandTimeout
    
TryAgain:
    
    mSQL = "SELECT 1 AS ConexionActiva"
    
    Set mRs = New ADODB.Recordset
    
    pConexion.CommandTimeout = 4
    
    ' DoEvents ' Mejor n�. No vaya a ser que otros eventos
    ' generados por el usuario provoquen acciones no deseadas.
    
    mRs.Open mSQL, pConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    'Debug.Print "Conexion Activa:", Not MRS.EOF
    
    ValidarConexion = True
    
Finally:
    
    pConexion.CommandTimeout = mCommandTimeOut
    
    Exit Function
    
Check:
    
    'Save Error properties.
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mDLLError = Err.LastDllError
    mErrorSource = Err.Source
    
    ' DoEvents ' Mejor n�. No vaya a ser que otros eventos
    ' generados por el usuario provoquen acciones no deseadas.
    
    Resume ResetErrorHandler
    
ResetErrorHandler:
    
    On Error GoTo Check
    
    If (mErrorNumber = (-2147467259) Or mErrorNumber = (3709)) And Not Reintento Then ' Error de Conexi�n / Error General de Red / Conexi�n cerrada.
        
        If pConexion.State <> adStateClosed Then pConexion.Close
        
        mConnectionString = IIf(Trim(pNewConnectionString) <> vbNullString, pNewConnectionString, pConexion.ConnectionString)
        
        If DebugMode2 Then
            MsgBox "Intentando Reestablecer conexi�n " & vbNewLine & mConnectionString
        End If
        
        Debug.Print "Intentando Reestablecer conexi�n", mConnectionString
        
        Reintento = True
        
        pConexion.ConnectionTimeout = TmpCnTimeOut
        
        pConexion.Open mConnectionString
        
        If DebugMode2 Then
            MsgBox "Conexi�n Reactivada " & vbNewLine & mConnectionString
        End If
        
        Debug.Print "Conexi�n Reactivada."
        
        'ExecuteSafeSQL "SET DATEFIRST " & DiaInicioSemanaSQL, pConexion
        
        Reintento = False 'Abrio la conexi�n... por lo tanto, vamos a hacer otro intento de validaci�n.
        
    Else ' Ante otros errores desconocidos / de otra causa, salir.
        Reintento = True
    End If
    
    If Not Reintento Then
        
        GoTo TryAgain
        
    Else
        
        If DebugMode2 Then
            MsgBox "Fallo al intentar reestablecer la conexi�n."
        End If
        
        Debug.Print "Fallo al intentar reestablecer la conexi�n."
        
        If pConexion.State = adStateClosed Then
            pConexion.ConnectionTimeout = mConnectionTimeOut
        End If
        
        ValidarConexion = False
        
    End If
    
    GoTo Finally
    
End Function
