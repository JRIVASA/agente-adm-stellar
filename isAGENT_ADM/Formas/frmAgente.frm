VERSION 5.00
Begin VB.Form frmAgente 
   Caption         =   "Stellar isAGENT_ADM"
   ClientHeight    =   6180
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8610
   Icon            =   "frmAgente.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6180
   ScaleWidth      =   8610
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstEstatus 
      Height          =   6105
      Left            =   30
      TabIndex        =   0
      Top             =   30
      Width           =   8535
   End
End
Attribute VB_Name = "frmAgente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public WithEvents fCls As clsAgente
Attribute fCls.VB_VarHelpID = -1
Private mBegin As Boolean
Private DiffFormVsListV As Integer
Private DiffFormVsListH As Integer

Const MinimoWidth As Integer = 4000
Const MinimoHeight As Integer = 4000

Private Sub fCls_EstatusAgente(pEstatus As String)
    Me.lstEstatus.AddItem pEstatus
    Me.lstEstatus.Refresh
    On Error Resume Next
    lstEstatus.ListIndex = lstEstatus.NewIndex
    DoEvents
End Sub

Private Sub fCls_IniciarLista()
    Me.lstEstatus.Clear
    DoEvents
End Sub

Private Sub fCls_Terminar()
    Unload Me
End Sub

Private Sub Form_Load()
     DoEvents
     DiffFormVsListV = Me.Height - Me.lstEstatus.Height
     DiffFormVsListH = Me.Width - Me.lstEstatus.Width
     Me.Icon = Form1.Icon
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then Cancel = 1
End Sub

Private Sub Form_Resize()
    
    Static mAjustando As Boolean
    
    If mAjustando Then Exit Sub
    
    On Local Error Resume Next
    
    mAjustando = True
    
    If Me.WindowState <> vbMinimized Then
        
        If AntWidth = 0 Then AntWidth = Me.Width
        If AntHeight = 0 Then AntHeight = Me.Height
        
        If Me.Height < MinimoHeight Then Me.Height = MinimoHeight
        If Me.Width < MinimoWidth Then Me.Width = MinimoWidth
        
        With lstEstatus
            .Height = Me.Height - DiffFormVsListV
            .Width = Me.Width - DiffFormVsListH
        End With
        
        Me.Refresh
        
    End If
    
    mAjustando = False
      
End Sub
