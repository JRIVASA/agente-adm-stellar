VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSucursal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mConexion                                                                               As ADODB.Connection
Private mVarCodigo                                                                              As String
Private mVarDescripcion                                                                         As String
Private mVarServidor                                                                            As String
Private mVar_BD_ADM                                                                             As String
Private mVar_BD_POS                                                                             As String
Private mVarTransmitir                                                                          As Boolean
Private mVarActivaVad10                                                                         As Boolean
Private mVarEstado                                                                              As Integer
Private mVarUser                                                                                As String
Private mVarPassword                                                                            As String
Private mTablasSincro                                                                           As Collection

Property Get Conexion() As ADODB.Connection
    Set Conexion = mConexion
End Property

Property Set Conexion(pCn As ADODB.Connection)
    Set mConexion = pCn
End Property

Property Get Codigo() As String
    Codigo = mVarCodigo
End Property

Property Get Descripcion() As String
    Descripcion = mVarDescripcion
End Property

Property Get Servidor() As String
    Servidor = mVarServidor
End Property

Property Get BD_ADM() As String
    BD_ADM = mVar_BD_ADM
End Property

Property Get BD_POS() As String
    BD_POS = mVar_BD_POS
End Property

Property Get Transmitir() As Boolean
    Transmitir = mVarTransmitir
End Property

Property Get ActualizaVad10() As Boolean
    ActualizaVad10 = mVarActivaVad10
End Property

Property Get Estado() As Integer
    Estado = mVarEstado
End Property

Property Get User() As String
    User = mVarUser
End Property

Property Get Password() As String
    Password = mVarPassword
End Property

Property Get TablasSincronizar() As Collection
    Set TablasSincronizar = mTablasSincro
End Property

Friend Function AgregarSucursal(pCn As ADODB.Connection, pRs As ADODB.Recordset) As clsSucursal
    
    Dim mArrDatos1
    
    mVarCodigo = pRs!c_Codigo
    mVarDescripcion = pRs!c_descripcion
    
    mVarServidor = pRs!c_servidor
    mArrDatos1 = Split(mVarServidor, "|", , vbTextCompare)
    
    If UBound(mArrDatos1) = 2 Then
        mVarServidor = mArrDatos1(0)
        mVar_BD_ADM = mArrDatos1(1)
        mVar_BD_POS = mArrDatos1(2)
    Else
        mVar_BD_ADM = mEstrucConex.sBD
        mVar_BD_POS = mEstrucConex.sBD_POS
    End If
    
    mVarTransmitir = pRs!B_TRASMITIR
    mVarActivaVad10 = pRs!B_ACTVAD10
    mVarEstado = pRs!c_estado
    
    Dim ContieneCamposLogin As Boolean
    
    ContieneCamposLogin = ExisteCampoRs(pRs, "Login") And ExisteCampoRs(pRs, "Password")
    
    If ContieneCamposLogin Then
        mVarUser = pRs!Login
        mVarPassword = pRs!Password
    Else
        mVarUser = vbNullString
        mVarPassword = vbNullString
    End If
    
    BuscarTablasSincronizar pCn
    
    Set AgregarSucursal = Me
    
End Function

Private Sub BuscarTablasSincronizar(pCn As ADODB.Connection)
    
    Dim mRs As ADODB.Recordset
    Dim mSql As String
    Dim mTbl As clsTablasSincronizar
    
    mSql = "SELECT DISTINCT m.cu_Descripcion, m.cu_Mensaje, m.cu_MensajeEliminar, " & _
    "m.cu_Tabla_Maestra, m.cu_CampoB_Maestra, m.cu_Tabla_Tr, m.cu_CampoB_Tr, " & _
    "m.cu_Tabla_Tr_Pend, m.cu_CampoB_TR_Pend, m.nu_Proceso, m.cu_CamposExepcion, " & _
    "m.cu_CamposExepcionTr, m.cu_CodTablasSyncronizar, m.bs_Maneja_Inventario, " & _
    "m.cu_Concepto " & _
    "FROM MA_TABLAS_SYNCRONIZAR AS m " & _
    "INNER JOIN TR_TABLAS_SYNCRONIZAR AS T " & _
    "ON m.cu_CodTablasSyncronizar = T.cu_CodTablasSyncronizar " & _
    "AND T.cu_CodSucursal = '" & mVarCodigo & "' "
    
    Set mTablasSincro = New Collection
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Do While Not mRs.EOF
        
        Set mTbl = New clsTablasSincronizar
        
        mTablasSincro.Add mTbl.AgregarTablaSincronizar(mRs)
        
        mRs.MoveNext
        
    Loop
    
    mRs.Close
    
End Sub

